# LIS4930

## Nikhil Bosshardt

### Assignment 3: Currency Converter App
### Convert between three different currencies and convert them.


*1. Currency Converter default screen:*

![localhost Screenshot](ss1.png = 500x)




*2. Screenshot of Currency Converter with invalid data:*

![localhost Screenshot](ss2.png =500x)




*3. Screenshot of Currency Converter with valid data:*

![localhost Screenshot](ss3.png =500x)



