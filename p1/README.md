# LIS4930

## Nikhil Bosshardt

### Project 1: My Music App
### Play and pause three different songs!


*1. Splash screen:*

![localhost Screenshot](ss1.png = 500x)




*2. Main activity with buttons and images:*

![localhost Screenshot](ss2.png =500x)




*3. Play/Pause/Reset Interface:*

![localhost Screenshot](ss3.png =500x)
![localhost Screenshot](ss4.png =500x)



