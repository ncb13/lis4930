# LIS4930

## Nikhil Bosshardt

### Assignment 5:

    - RSS news feed app
    - SAX RSS Libraries used
    - Scrolling lists, file writing, and web browsing also included!



*1. List View:*

![localhost Screenshot](ss1.png)

*2. Item View:*

![localhost Screenshot](ss2.png)

*3. Web View*

![localhost Screenshot](ss3.png)
