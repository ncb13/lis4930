# LIS4930

## Nikhil Bosshardt

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install android studio.
    - Two basic apps running.

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Split the bill app.
    - Handy app for calculating the total cost of a bill with tip for many poeple.
    - Using dropdown menus and buttons.

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Currency Converter Application running.
    - Re-positioned toasts.
    - Radio buttons and radio groups.

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - My Music app running
    - Clean, low code, splash screen.
    - Media player player with play, pause, reset, and three tracks.

5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Mortgage Interest Rate Calculator
    - See shared prefs in the hints!
    - Also includes splash screen and some error checking.

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - RSS news feed app
    - SAX RSS Libraries used
    - Scrolling lists, file writing, and web browsing also included!

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Task List App
    - SQLite database with create/update/delete/read functionality
    - Can store all your tasks... no working GUI yet.