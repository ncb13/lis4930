# LIS4930

## Nikhil Bosshardt

### Assignment 1 Requirments:

* Screenshot of running JDK java Hello
* Screenshot of running Android Studio My First App
* Screenshot of contacts app running
* git commands w/short descriptions (“Creating a New Repository” above);
* Bitbucket repo links: a) this assignment and b) the completed tutorial repos above (bitbucketstationlocations and myteamquotes).



*1. Screenshot of JDK java hello:*

![localhost Screenshot](images/ss1.JPG)

*2. Screenshot of Android Studio my first app:*

![localhost Screenshot](images/ss2.JPG)

*3. Screenshot of contacts app:*

![localhost Screenshot](images/ss3.png)
![localhost Screenshot](images/ss4.png)

*4. Git commands w/short descriptions:*

* git init - Creates an empty git repo.
* git status - Shows the current repo status, what you have and have not sent etc.
* git add - Adds a file to the index to be pushed/commited.
* git commit - Record changes to your repo.
* git push - Updates your remote repo.
* git pull - Updates your local repo.
* git rm - removes files from your index.

*5. Bitbucket Repo Links:*

1. https://bitbucket.org/ncb13/lis4381
2. Bitbucket Tutorial - Station Locations:
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ncb13/bitbucketstationlocations/ "Bitbucket Station Locations")
3. Tutorial: Request to update a teammate's repository:
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ncb13/myteamquotes/ "My Team Quotes Tutorial")