# LIS4930

## Nikhil Bosshardt

### Assignment 4:

    - Mortgage Interest Rate Calculator
    - See shared prefs in the hints!
    - Also includes splash screen and some error checking.



*1. Calculator default screen after splash:*

![localhost Screenshot](ss1.png)

*2. When the year does not pass error checking:*

![localhost Screenshot](ss2.png)

*3. Response with all data entered correctly:*

![localhost Screenshot](ss3.png)
